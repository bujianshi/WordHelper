package ncist.edu.cn.view;


import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;


public class SoftKeyBoardStatusView extends LinearLayout {

    private SoftKeyBoardListener boardListener;

    private final int CHANGE_SIZE = 100;

    public SoftKeyBoardStatusView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (oldw == 0 || oldh == 0) {
            return;
        }
        if (boardListener != null) {
            boardListener.keyBoardStatus(w, h, oldw, oldh);
            if (oldw != 0 && h - oldh < -CHANGE_SIZE) {
                boardListener.keyBoardVisible(Math.abs(h - oldh));
            }

            if (oldw != 0 && h - oldh > CHANGE_SIZE) {
                boardListener.keyBoardInvisible(Math.abs(h - oldh));
            }
        }
    }

    public interface SoftKeyBoardListener {

        void keyBoardStatus(int w, int h, int oldw, int oldh);

        void keyBoardVisible(int move);

        void keyBoardInvisible(int move);

    }

    public void setSoftKeyBoardListener(SoftKeyBoardListener boardListener) {
        this.boardListener = boardListener;
    }
}