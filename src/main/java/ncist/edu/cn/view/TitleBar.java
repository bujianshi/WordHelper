package ncist.edu.cn.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.TextView;
import ncist.edu.cn.R;

public class TitleBar extends RelativeLayout {

    /**
     * TitleBar的TextView的控件
     */
    private TextView mTitleBarTextView;
    /**
     * TitleBar的TextView中的显示标题文字
     */
    private String mTitleBarText;
    /**
     * TitleBar的TextView中的显示标题文字的大小
     */
    private float mTitleBarTextSize;
    /**
     * TitleBar的TextView中的显示标题文字的颜色
     */
    private int mTitleBarTextColor;
    /**
     * 标题栏左侧的ImageView
     */
    private ImageView mIndicatorImageView;
    /**
     * 标题栏左侧的图片
     */
    private Drawable mIndicatorDrawable;
    /**
     * 是否显示Indicator
     */
    private boolean mIndicatorVisibility;
    /**
     * 标题栏右侧的按钮
     */
    private TextView mTitleBarButton;
    /**
     * 标题栏右侧的按钮中文字
     */
    private String mTitleBarButtonText;
    /**
     * 标题栏右侧按钮的文字大小
     */
    private float mTitleBarButtonTextSize;
    /**
     * 标题栏右侧的按钮中文字颜色
     */
    private int mTitleBarButtonTextColor;
    /**
     * 标题栏右侧的按钮是否显示
     */
    private boolean mTitleBarButtonVisibility;

    public TitleBar(Context context) {
        this(context, null);
    }

    public TitleBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TitleBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        initTitleData(context, attrs);

        initTitleBarTitleText(context);

        initTitleBarTitleImage(context);

        initTitleBarButton(context);

    }

    private void initTitleBarButton(Context context) {
        // 初始化Button的Layout属性
        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        // 设置Button垂直居中,右对齐
        params.addRule(CENTER_VERTICAL);
        params.addRule(ALIGN_PARENT_RIGHT);
        // 创建Button的TextView
        mTitleBarButton = new TextView(context);
        // 设置Button的Layout属性
        mTitleBarButton.setLayoutParams(params);
        mTitleBarButton.setPadding(0, 0, 20, 0);
        // 设置Button的文字
        mTitleBarButton.setText(mTitleBarButtonText);
        // 设置Button的文字大小
        mTitleBarButton.setTextSize(mTitleBarButtonTextSize);
        // 设置Button的文字颜色
        mTitleBarButton.setTextColor(mTitleBarButtonTextColor);
        // 设置Button是否被显示
        setButtonVisibility(mTitleBarButtonVisibility);
        // 设置Button的监听事件
        mTitleBarButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mOnButtonClickListener != null) {
                    mOnButtonClickListener.onButtonClick();
                }
            }
        });
        // 将Button添加到TitleBar
        addView(mTitleBarButton);
    }

    private void initTitleBarTitleImage(Context context) {
        // 初始化Indicator的Layout属性
        LayoutParams params = new LayoutParams(100, 100);
        // 设置Indicator垂直居中
        params.addRule(CENTER_VERTICAL);
        // 创建Indicator的ImageView
        mIndicatorImageView = new ImageView(context);
        // 设置Indicator的Layout属性
        mIndicatorImageView.setLayoutParams(params);
        // 设置Indicator的ImageView相关属性
        mIndicatorImageView.setScaleType(ScaleType.FIT_CENTER);
        // 设置Indicator的图片
        mIndicatorImageView.setImageDrawable(mIndicatorDrawable);
        // 设置Indicator的padding值
        mIndicatorImageView.setPadding(50,0,0,0);
        // 设置Indicator的可见性
        setIndicatorVisibility(mIndicatorVisibility);
        // 配置Indicator的点击监听
        mIndicatorImageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnIndicatorClickListener != null) {
                    mOnIndicatorClickListener.onIndicatorClick();
                }
            }
        });
        // 将ImageView添加到TitleBar
        addView(mIndicatorImageView);
    }

    /**
     * 读取在Layout中或者Style中配置的属性
     *
     * @param context
     *            上下文对象
     * @param attrs
     */

    private void initTitleData(Context context, AttributeSet attrs) {
        TypedArray ta = context.obtainStyledAttributes(attrs,
                R.styleable.TitleBar);
        // 读取标题文字
        mTitleBarText = ta.getString(R.styleable.TitleBar_title_text);
        // 读取标题文字的尺寸
        mTitleBarTextSize = ta.getDimension(
                R.styleable.TitleBar_title_text_size, 18);
        // 读取标题文字的颜色
        mTitleBarTextColor = ta.getColor(R.styleable.TitleBar_title_text_color,
                Color.BLACK);
        // 读取左侧图片是否显示
        mIndicatorVisibility = ta.getBoolean(
                R.styleable.TitleBar_show_indicator, false);
        // 读取显示的图片的资源
        mIndicatorDrawable = ta
                .getDrawable(R.styleable.TitleBar_indicator_image_drawable);
        // 读取右侧的按钮文字
        mTitleBarButtonText = ta.getString(R.styleable.TitleBar_button_text);
        // 读取右侧的按钮文字大小
        mTitleBarButtonTextSize = ta.getDimension(
                R.styleable.TitleBar_button_text_size, 10);
        // 读取右侧的按钮文字颜色
        mTitleBarButtonTextColor = ta.getColor(
                R.styleable.TitleBar_button_text_color, Color.BLACK);
        // 读取右侧的按钮是否被显示
        mTitleBarButtonVisibility = ta.getBoolean(
                R.styleable.TitleBar_show_button, false);

        // 释放资源
        ta.recycle();
    }

    /**
     * 配置TitleBar中标题各属性
     *
     * @param context
     *            上下文对象
     */
    private void initTitleBarTitleText(Context context) {
        // 初始化标题TextView的Layout属性
        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        // 设置标题的对齐方式
        params.addRule(CENTER_IN_PARENT);
        // 初始化标题TextView
        mTitleBarTextView = new TextView(context);
        // 设置标题TextView的尺寸
        mTitleBarTextView.setLayoutParams(params);
        // 设置标题文字
        mTitleBarTextView.setText(mTitleBarText);
        // 设置标题文字大小
        mTitleBarTextView.setTextSize(mTitleBarTextSize);
        // 设置标题文字颜色
        mTitleBarTextView.setTextColor(mTitleBarTextColor);
        // 将标题TextView添加到当前TitleBar中
        addView(mTitleBarTextView);
    }

    private void setIndicatorVisibility(boolean visibility) {
        if (mIndicatorImageView != null) {
            mIndicatorImageView.setVisibility(visibility ? View.VISIBLE
                    : View.GONE);
        }
    }

    private void setButtonVisibility(boolean visibility) {
        if (mTitleBarButton != null) {
            mTitleBarButton
                    .setVisibility(visibility ? View.VISIBLE : View.GONE);
        }
    }

    /**
     * Indicator的点击监听
     */
    private OnIndicatorClickListener mOnIndicatorClickListener;

    /**
     * Button的点击监听
     */
    private OnButtonClickListener mOnButtonClickListener;

    /**
     * 设置Indicator的点击监听
     *
     * @param listener
     *            Indicator的点击监听器对象
     */
    public void setOnIndicatorClickListener(OnIndicatorClickListener listener) {
        mOnIndicatorClickListener = listener;
    }

    /**
     * 设置Button的点击监听
     *
     * @param listener
     */
    public void setOnButtonClickListener(OnButtonClickListener listener) {
        mOnButtonClickListener = listener;
    }

    /**
     *
     * Indicator的点击监听
     */
    public static interface OnIndicatorClickListener {
        /**
         * Indicator的点击监听
         */
        void onIndicatorClick();
    }

    /**
     * Button的点击监听
     *
     */
    public static interface OnButtonClickListener {
        /**
         * Button的点击监听
         *
         */
        void onButtonClick();
    }

}
