package ncist.edu.cn.utils;


import android.content.Context;

import java.io.File;

public class FilePathUtils {

    public static void makeFilePath(Context context, String pathName) {
        String filePath = context.getFilesDir().getAbsolutePath();
        filePath = filePath.substring(0, filePath.lastIndexOf("/") + 1);
        filePath = filePath + pathName;
        File file = new File(filePath);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    public static String getFilePath(Context context,String pathName){
        String filePath = context.getFilesDir().getAbsolutePath();
        filePath = filePath.substring(0, filePath.lastIndexOf("/") + 1)+ pathName;
        return filePath;
    }
}
