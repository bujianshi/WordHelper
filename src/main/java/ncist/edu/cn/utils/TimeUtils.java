package ncist.edu.cn.utils;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TimeUtils {
    public static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss",
            Locale.CHINA);

    public static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd",
            Locale.CHINA);

    public static long getTimeByString(String time) {
        try {
            return format.parse(time).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * 获取年月日字符串
     */
    public static String getSimpleDateStringTimeNow() {
        return simpleDateFormat.format(new Date(System.currentTimeMillis()));
    }
}
