package ncist.edu.cn.utils;


import android.app.Activity;
import android.content.Intent;
import android.provider.MediaStore;

public class IntentUtils {
    public static void selectPicture(Activity activity) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_PICK);
        // 设置类型
        intent.setType("image/*");
        Intent intent2 = new Intent();
        intent2.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
        Intent chooser = Intent.createChooser(intent, "请选择...");
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] { intent2 });
        activity.startActivityForResult(chooser,2);
    }
}
