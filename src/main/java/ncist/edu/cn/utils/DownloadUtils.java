package ncist.edu.cn.utils;


import android.os.Handler;
import android.os.Message;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class DownloadUtils {

    /**
     * 进度这里固定为100
     */
    public static final int jd = 100;

    public static void download(final String urlStr, final String path, final String fileName,
                                final Handler handler) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                File dir = new File(path);
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                File file = new File(dir, fileName);
                InputStream is;
                OutputStream os;
                try {
                    // 构造URL
                    URL url = new URL(urlStr);
                    // 打开连接
                    URLConnection con = url.openConnection();
                    //获得文件的长度
                    int contentLength = con.getContentLength();
                    int cd = 0;
                    if (contentLength >= jd) {
                        cd = contentLength / jd;//计算出文件长度的1/100用于进度
                    }
                    // 输入流
                    is = con.getInputStream();
                    // 1K的数据缓冲
                    byte[] bs = new byte[1024];
                    // 读取到的数据长度
                    int len;
                    // 输出的文件流
                    os = new FileOutputStream(file);
                    // 开始读取
                    int i = 0;
                    int arg = 0;
                    while ((len = is.read(bs)) != -1) {
                        os.write(bs, 0, len);
                        i += len;
                        if (i >= cd) {
                            Message msg = Message.obtain();
                            msg.arg1 = ++arg;
                            msg.what = 2;
                            handler.sendMessage(msg);
                            i = 0;
                        }
                    }
                    // 完毕，关闭所有链接
                    os.close();
                    is.close();
                    handler.sendEmptyMessage(1);
                } catch (Exception e) {
                    e.printStackTrace();
                    handler.sendEmptyMessage(0);
                }
            }
        }).start();
    }
}
