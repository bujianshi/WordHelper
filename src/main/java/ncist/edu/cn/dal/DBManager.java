package ncist.edu.cn.dal;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import ncist.edu.cn.R;

/**
 * 对词典数据库的管理
 *
 * @author tarena
 *
 */
public class DBManager {
    private Context context;
    private SQLiteDatabase database;
    private String DB_NAME;

    public DBManager(Context context) {
        super();
        this.context = context;
        // ncist.edu.cn:raw/word
        DB_NAME = context.getResources().getResourceName(R.raw.word);
        DB_NAME = DB_NAME.substring(DB_NAME.lastIndexOf("/") + 1,
                DB_NAME.length())
                + ".db";
    }

    /**
     * 得到打开词典数据库对象
     */
    public SQLiteDatabase openDatabase() {
        File file = null;
        File dir = context.getDatabasePath(DB_NAME).getParentFile();
        InputStream in = null;
        FileOutputStream fos = null;
        if (!dir.exists()) {
            dir.mkdirs();
        }
        // 判断数据库文件是否存在，若不存在则执行导入，否则直接打开数据库
        file = new File(dir, DB_NAME);
        if (!file.exists()) {
            in = context.getResources().openRawResource(R.raw.word);
            try {
                fos = new FileOutputStream(file);
                byte[] buffer = new byte[8 * 1024];
                int len = -1;
                while ((len = in.read(buffer)) != -1) {
                    fos.write(buffer, 0, len);
                    fos.flush();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (in != null) {
                        in.close();
                    }
                    if (fos != null) {
                        fos.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        database = SQLiteDatabase.openOrCreateDatabase(file, null);
        return database;
    }

    /**
     * 关闭数据库
     */
    public void closeDatabase() {
        if (database != null) {
            database.close();
        }
    }
}
