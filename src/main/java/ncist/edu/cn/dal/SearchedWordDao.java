package ncist.edu.cn.dal;


import android.content.Context;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import ncist.edu.cn.entity.SearchedWord;
import ncist.edu.cn.utils.DBUtils;

public class SearchedWordDao {
    private Dao<SearchedWord, Integer> dao;

    public SearchedWordDao(Context context) {
        try {
            dao = DBUtils.newInstance(context).getDao(SearchedWord.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int add(SearchedWord searchedWord) throws SQLException{
        return dao.create(searchedWord);
    }

    public int deleteAll(List<SearchedWord> searchedWords) throws SQLException{
        return dao.delete(searchedWords);
    }

    public int delete(SearchedWord searchedWord) throws SQLException{
        return dao.delete(searchedWord);
    }

    public int delete(int id) throws SQLException{
        return dao.deleteById(id);
    }

    public int update(SearchedWord searchedWord) throws SQLException{
        return dao.update(searchedWord);
    }

    public List<SearchedWord> queryAll() throws SQLException{
        return dao.queryForAll();
    }

    public SearchedWord queryBySearchedWord(SearchedWord searchedWord) throws SQLException{
        return dao.queryForSameId(searchedWord);
    }

    public SearchedWord queryById(int id) throws SQLException{
        return dao.queryForId(id);
    }

    public List<SearchedWord> queryForMap(Map<String,Object> map) throws SQLException {
        return dao.queryForFieldValues(map);
    }
}
