package ncist.edu.cn.dal;

import android.content.Context;

import java.util.Map;
import java.util.Set;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.listener.FindListener;

public class BmobDao {

    public static <T> void queryByMap(Context context, Map<String,Object> map ,Class<T> clazz, FindListener<T> findListener){
        BmobQuery<T> query = new BmobQuery<>();
        Set<String> keys =  map.keySet();
        for (String str :keys){
            query.addWhereEqualTo(str,map.get(str));
        }
        query.findObjects(context,findListener);

    }
}