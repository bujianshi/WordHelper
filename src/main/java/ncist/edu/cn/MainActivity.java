package ncist.edu.cn;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ncist.edu.cn.fragment.DictionaryFragment;
import ncist.edu.cn.fragment.ReciteFragment;
import ncist.edu.cn.fragment.SettingsFragment;
import ncist.edu.cn.fragment.TranslationFragment;
import ncist.edu.cn.utils.CommonUtils;
import ncist.edu.cn.utils.SharedPreferencesUtils;

public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {


    @Bind(R.id.main_tv_01)
    TextView mainTv01;
    @Bind(R.id.main_tv_02)
    TextView mainTv02;
    @Bind(R.id.main_tv_03)
    TextView mainTv03;
    @Bind(R.id.main_tv_04)
    TextView mainTv04;
    @Bind(R.id.main_tv_11)
    TextView mainTv11;
    @Bind(R.id.main_tv_12)
    TextView mainTv12;
    @Bind(R.id.main_tv_13)
    TextView mainTv13;
    @Bind(R.id.main_tv_14)
    TextView mainTv14;
    @Bind(R.id.main_viewpager)
    ViewPager mainViewpager;

    private PagerAdapter adapter;
    private int lastPosition;
    private long firstPress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        adapter = new InnerViewPager(getSupportFragmentManager());
        mainViewpager.setAdapter(adapter);
        mainViewpager.addOnPageChangeListener(this);
        mainViewpager.setOffscreenPageLimit(5);
    }


    @OnClick({R.id.main_tv_01, R.id.main_tv_02, R.id.main_tv_03, R.id.main_tv_04, R.id.main_tv_11, R.id.main_tv_12, R.id.main_tv_13, R.id.main_tv_14})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.main_tv_01:
                mainViewpager.setCurrentItem(0);
                break;
            case R.id.main_tv_02:
                mainViewpager.setCurrentItem(1);
                break;
            case R.id.main_tv_03:
                mainViewpager.setCurrentItem(2);
                break;
            case R.id.main_tv_04:
                mainViewpager.setCurrentItem(3);
                break;
            case R.id.main_tv_11:
                mainViewpager.setCurrentItem(0);
                break;
            case R.id.main_tv_12:
                mainViewpager.setCurrentItem(1);
                break;
            case R.id.main_tv_13:
                mainViewpager.setCurrentItem(2);
                break;
            case R.id.main_tv_14:
                mainViewpager.setCurrentItem(3);
                break;
        }
    }

    private class InnerViewPager extends FragmentPagerAdapter {

        public InnerViewPager(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position) {
                case 0:
                    fragment = new ReciteFragment();
                    break;
                case 1:
                    fragment = new DictionaryFragment();
                    break;
                case 2:
                    fragment = new TranslationFragment();
                    break;
                case 3:
                    fragment = new SettingsFragment();
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 4;
        }



    }

    //当fragment页面被选择的时候
    @Override
    public void onPageSelected(int position) {
        switch (lastPosition) {
            case 0:
                mainTv01.setTextColor(ContextCompat.getColor(this, R.color.lightWhite));
                mainTv11.setBackgroundColor(ContextCompat.getColor(this, R.color.black));
                break;
            case 1:
                mainTv02.setTextColor(ContextCompat.getColor(this, R.color.lightWhite));
                mainTv12.setBackgroundColor(ContextCompat.getColor(this, R.color.black));
                break;
            case 2:
                mainTv03.setTextColor(ContextCompat.getColor(this, R.color.lightWhite));
                mainTv13.setBackgroundColor(ContextCompat.getColor(this, R.color.black));
                break;
            case 3:
                mainTv04.setTextColor(ContextCompat.getColor(this, R.color.lightWhite));
                mainTv14.setBackgroundColor(ContextCompat.getColor(this, R.color.black));
                break;
        }
        switch (position) {
            case 0:
                mainTv01.setTextColor(ContextCompat.getColor(this, R.color.white));
                mainTv11.setBackgroundColor(ContextCompat.getColor(this, R.color.lightBlue));
                break;
            case 1:
                mainTv02.setTextColor(ContextCompat.getColor(this, R.color.white));
                mainTv12.setBackgroundColor(ContextCompat.getColor(this, R.color.lightBlue));
                break;
            case 2:
                mainTv03.setTextColor(ContextCompat.getColor(this, R.color.white));
                mainTv13.setBackgroundColor(ContextCompat.getColor(this, R.color.lightBlue));
                break;
            case 3:
                mainTv04.setTextColor(ContextCompat.getColor(this, R.color.white));
                mainTv14.setBackgroundColor(ContextCompat.getColor(this, R.color.lightBlue));
                break;
        }
        lastPosition = position;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    protected void onDestroy() {
        SharedPreferencesUtils.putBoolean(this,"isLogin",false);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if(firstPress+2000>System.currentTimeMillis()){
            SharedPreferencesUtils.putString(this,"userId",null);
            SharedPreferencesUtils.putString(this,"mobile",null);
            SharedPreferencesUtils.putBoolean(this,"isLogin",false);
            finish();
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(0);
        }else{
            CommonUtils.showToast(this,"再按一次退出");
        }
        firstPress = System.currentTimeMillis();
    }

}
