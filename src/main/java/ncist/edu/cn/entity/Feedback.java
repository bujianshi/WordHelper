package ncist.edu.cn.entity;

import cn.bmob.v3.BmobObject;

public class Feedback extends BmobObject {

    private String email;

    private String suggestion;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

    @Override
    public String toString() {
        return "Feedback{" +
                "email='" + email + '\'' +
                ", suggestion='" + suggestion + '\'' +
                '}';
    }
}
