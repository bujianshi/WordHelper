package ncist.edu.cn.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmob.v3.listener.SaveListener;
import ncist.edu.cn.R;
import ncist.edu.cn.entity.Feedback;
import ncist.edu.cn.utils.CommonUtils;
import ncist.edu.cn.utils.ValidatorUtil;
import ncist.edu.cn.view.TitleBar;


public class FeedbackActivity extends AppCompatActivity implements TitleBar.OnIndicatorClickListener {

    @Bind(R.id.feedback_titlebar)
    TitleBar feedbackTitlebar;
    @Bind(R.id.feedback_et_email)
    EditText feedbackEtEmail;
    @Bind(R.id.feedback_et_suggestion)
    EditText feedbackEtSuggestion;
    @Bind(R.id.feedback_btn_submit)
    Button feedbackBtnSubmit;

    private String email;
    private String suggestion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        ButterKnife.bind(this);
        feedbackTitlebar.setOnIndicatorClickListener(this);
    }

    @OnClick(R.id.feedback_btn_submit)
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.feedback_btn_submit:
                email = feedbackEtEmail.getText().toString().trim();
                if (!ValidatorUtil.isEmail(email)) {
                    CommonUtils.showToast(this, "请输入合法的邮箱!");
                    return;
                }
                suggestion = feedbackEtSuggestion.getText().toString().trim();
                if (ValidatorUtil.isEmpty(suggestion)) {
                    CommonUtils.showToast(this, "建议不能为空!");
                    return;
                }
                submit(email,suggestion);
                break;
        }
    }

    private void submit(String email, String suggestion) {
        Feedback feedback = new Feedback();
        feedback.setEmail(email);
        feedback.setSuggestion(suggestion);
        feedback.save(this, new SaveListener() {
            @Override
            public void onSuccess() {
                CommonUtils.showToast(FeedbackActivity.this,"提交成功!");
                finish();
            }

            @Override
            public void onFailure(int i, String s) {
                CommonUtils.showToast(FeedbackActivity.this,"提交失败!");
            }
        });
    }

    @Override
    public void onIndicatorClick() {
        finish();
    }
}
