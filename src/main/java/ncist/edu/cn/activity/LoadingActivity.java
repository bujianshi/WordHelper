package ncist.edu.cn.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import ncist.edu.cn.R;
import ncist.edu.cn.utils.Constant;
import ncist.edu.cn.utils.SharedPreferencesUtils;

public class LoadingActivity extends AppCompatActivity {

    private ImageView imageView;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        imageView = (ImageView) findViewById(R.id.loading_imageview);
        handler = new Handler();
        handler.postDelayed(new Thread() {
            @Override
            public void run() {
                chooseIntent();
            }
        }, 1000);
    }

    public void chooseIntent() {
        boolean isFirstOpen = SharedPreferencesUtils.getBoolean(this, Constant.IS_FIRST_OPEN);
        Intent intent;
        if (isFirstOpen) {
            intent = new Intent(this, WelcomeActivity.class);
        } else {
            intent = new Intent(this, LoginActivity.class);
        }
        startActivity(intent);
        finish();
    }
}
