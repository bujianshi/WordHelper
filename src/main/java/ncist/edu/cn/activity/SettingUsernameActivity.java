package ncist.edu.cn.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.UpdateListener;
import ncist.edu.cn.R;
import ncist.edu.cn.entity.MyUser;
import ncist.edu.cn.utils.CommonUtils;
import ncist.edu.cn.utils.SharedPreferencesUtils;
import ncist.edu.cn.view.TitleBar;

import static ncist.edu.cn.dal.BmobDao.queryByMap;

public class SettingUsernameActivity extends AppCompatActivity implements TitleBar.OnIndicatorClickListener {

    @Bind(R.id.setting_username_titlebar)
    TitleBar settingUsernameTitlebar;
    @Bind(R.id.setting_username_et_username)
    EditText settingUsernameEtUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_username);
        ButterKnife.bind(this);
        settingUsernameTitlebar.setOnIndicatorClickListener(this);
    }

    @OnClick(R.id.setting_username_btn_submit)
    public void onClick() {
        final String username = settingUsernameEtUsername.getText().toString();
        if (username.length() == 0) {
            CommonUtils.showToast(this, "用户名不能为空!");
            return;
        } else {
            Map<String, Object> map = new HashMap<>();
            String userId = SharedPreferencesUtils.getString(this, "userId");
            map.put("userId", userId);
            queryByMap(this, map, MyUser.class, new FindListener<MyUser>() {
                @Override
                public void onSuccess(List<MyUser> list) {
                    if (list != null && list.size() > 0) {
                        MyUser myUser = list.get(0);
                        myUser.setUsername(username);
                        myUser.update(SettingUsernameActivity.this, new UpdateListener() {
                            @Override
                            public void onSuccess() {
                                CommonUtils.showToast(SettingUsernameActivity.this,"修改用户名成功!");
                                Intent intent = new Intent();
                                intent.putExtra("username",username);
                                setResult(RESULT_OK,intent);
                                finish();
                            }

                            @Override
                            public void onFailure(int i, String s) {
                                Log.e("error",s);
                            }
                        });
                    }
                }

                @Override
                public void onError(int i, String s) {

                }
            });
        }
    }

    @Override
    public void onIndicatorClick() {
        finish();
    }
}
