package ncist.edu.cn.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.listener.FindListener;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.wechat.friends.Wechat;
import ncist.edu.cn.MainActivity;
import ncist.edu.cn.R;
import ncist.edu.cn.application.MyApplication;
import ncist.edu.cn.entity.MyUser;
import ncist.edu.cn.utils.Constant;
import ncist.edu.cn.utils.DialogUtils;
import ncist.edu.cn.utils.SharedPreferencesUtils;
import ncist.edu.cn.view.CircleImageView;
import ncist.edu.cn.view.SoftKeyBoardStatusView;


public class LoginActivity extends AppCompatActivity implements SoftKeyBoardStatusView
        .SoftKeyBoardListener, View.OnClickListener,

        PlatformActionListener {

    @Bind(R.id.login_iv_qq)
    ImageView loginIvQq;
    @Bind(R.id.login_iv_weixin)
    ImageView loginIvWeixin;
    @Bind(R.id.login_iv_weibo)
    ImageView loginIvWeibo;
    @Bind(R.id.login_iv_logo)
    CircleImageView loginIvLogo;

    private SoftKeyBoardStatusView softKeyBoardStatusView;
    private Button btnLogin;
    private EditText etMobile;
    private EditText etPassword;
    private TextView tvRegister;
    //屏幕滚动距离
    private int scroll_dx;
    private LinearLayout linearLayout;
    private RelativeLayout relativeLayout;
    private TextView tvforgetPwd;
    private Dialog dialog;
    private MyApplication app;
    private final int REGISTER_SUCCESS = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        init();
        ShareSDK.initSDK(this);
    }

    //初始化控件
    private void init() {
        softKeyBoardStatusView = (SoftKeyBoardStatusView) findViewById(R.id.login_skdsv);
        softKeyBoardStatusView.setSoftKeyBoardListener(this);
        btnLogin = (Button) findViewById(R.id.login_btn_login);
        linearLayout = (LinearLayout) findViewById(R.id.login_ll_login);
        relativeLayout = (RelativeLayout) findViewById(R.id.login_rl_login);
        etMobile = (EditText) findViewById(R.id.login_et_phone);
        etPassword = (EditText) findViewById(R.id.login_et_password);
        tvRegister = (TextView) findViewById(R.id.login_tv_register);
        tvforgetPwd = (TextView) findViewById(R.id.login_tv_forget_pwd);
        tvRegister.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        tvforgetPwd.setOnClickListener(this);
        etMobile.setBackgroundResource(R.color.smallLightBlack);
        etPassword.setBackgroundResource(R.color.smallLightBlack);
        etMobile.setText("");
        etPassword.setText("");
        Constant.SCREEN_HEIGHT = getScreenHeight();
        app = (MyApplication) getApplication();
    }

    //获取屏幕高度
    private int getScreenHeight() {
        int height;
        //获取窗口对象
        WindowManager manager = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        //定义DisplayMetrics对象
        DisplayMetrics metrics = new DisplayMetrics();
        //取得窗口属性
        manager.getDefaultDisplay().getMetrics(metrics);
        //窗口高度
        height = metrics.heightPixels;
        return height;
    }

    @OnClick({R.id.login_iv_qq, R.id.login_iv_weixin, R.id.login_iv_weibo})
    @Override
    public void onClick(View v) {
        final String mobile = etMobile.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        MyUser user = new MyUser();
        user.setMobile(mobile);
        user.setPassword(password);
        switch (v.getId()) {
            //登录
            case R.id.login_btn_login:
                dialog = DialogUtils.createLoadingDialog(this, "登录中...");
                if (!checkLoginInput(mobile, password)) {
                    return;
                }
                BmobQuery<MyUser> query = new BmobQuery<MyUser>();
                query.addWhereEqualTo("mobile", mobile);
                query.addWhereEqualTo("password", password);
                query.findObjects(this, new FindListener<MyUser>() {
                    @Override
                    public void onSuccess(List<MyUser> list) {
                        if (list != null && list.size() > 0) {
                            SharedPreferencesUtils.putString(LoginActivity.this, "userId", list
                                    .get(0).getUserId());
                            SharedPreferencesUtils.putString(LoginActivity.this, "mobile", mobile);
                            SharedPreferencesUtils.putBoolean(LoginActivity.this, "isLogin", true);
                            Constant.MOBILE = mobile;
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                            app.setCurrentUser(list.get(0));
                            finish();
                        } else {
                            Toast.makeText(LoginActivity.this, "登录失败!", Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(int i, String s) {
                        dialog.dismiss();
                        Log.e("error", i + s);
                    }
                });
                break;
            //注册
            case R.id.login_tv_register:
                Intent intent = new Intent(this, RegisterActivity.class);
                startActivityForResult(intent,REGISTER_SUCCESS);
                break;
            //忘记密码
            case R.id.login_tv_forget_pwd:
                intent = new Intent(this, FindPasswordActivity.class);
                startActivity(intent);
                break;
            case R.id.login_iv_qq:
                authorize(new QQ(this));
                break;
            case R.id.login_iv_weixin:
                authorize(new Wechat(this));
                break;
            case R.id.login_iv_weibo:
                authorize(new SinaWeibo(this));
                break;
        }
    }

    public boolean checkLoginInput(String mobile, String password) {
        if (mobile.length() != 11) {
            Toast.makeText(this, "手机号长度不符合要求!", Toast.LENGTH_SHORT).show();
            hideDialog();
            return false;
        }
        if (password.length() < 6) {
            Toast.makeText(this, "密码长度小于6位!", Toast.LENGTH_SHORT).show();
            hideDialog();
            return false;
        }
        return true;
    }

    public void authorize(Platform platform) {
        if (platform.isAuthValid()) {
            platform.removeAccount();
        }
        platform.SSOSetting(false);//优先使用客户端授权
        platform.setPlatformActionListener(this);
//        platform.authorize();//授权
        platform.showUser(null);
        dialog = DialogUtils.createLoadingDialog(this, "登录中...");
    }


    @Override
    public void keyBoardStatus(int w, int h, int oldw, int oldh) {

    }

    @Override
    public void keyBoardVisible(int move) {
        loginIvLogo.setVisibility(View.INVISIBLE);
        int[] location = new int[2];
        relativeLayout.getLocationOnScreen(location);
        int tvToBottom = Constant.SCREEN_HEIGHT - location[1] - relativeLayout.getHeight();
        scroll_dx = tvToBottom > move ? 0 : move - tvToBottom;
        linearLayout.scrollBy(0, scroll_dx);
    }

    @Override
    public void keyBoardInvisible(int move) {
        loginIvLogo.setVisibility(View.VISIBLE);
        linearLayout.scrollBy(0, -scroll_dx);
    }

    @Override
    public void onComplete(Platform platform, int action, HashMap<String, Object> hashMap) {
        if (action == Platform.ACTION_USER_INFOR) {
            String username = platform.getDb().getUserName();
            SharedPreferencesUtils.putString(this, "platformUsername", username);
            String userId = platform.getDb().getUserId();
            String icon = platform.getDb().getUserIcon();
            MyUser user = new MyUser();
            user.setUserId(userId);
            user.setIcon(icon);
            user.setUsername(username);
            if (platform.isValid()) {//授权完毕
                //将登录状态改为true,缓存用户信息
                SharedPreferencesUtils.putString(this,"userId",userId);
                SharedPreferencesUtils.putBoolean(this, "isLogin", true);
                SharedPreferencesUtils.putString(this, "mobile", "null");
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                app.setCurrentUser(user);
                finish();
                hideDialog();
            }
        }
    }

    @Override
    public void onError(Platform platform, int action, Throwable throwable) {
        hideDialog();
    }

    @Override
    public void onCancel(Platform platform, int action) {
        hideDialog();
    }

    public void hideDialog() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            switch (requestCode){
                case REGISTER_SUCCESS:
                    String mobile = data.getStringExtra("mobile");
                    String password = data.getStringExtra("password");
                    etMobile.setText(mobile);
                    etPassword.setText(password);
                    break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        ShareSDK.stopSDK(this);
        super.onDestroy();
    }
}
