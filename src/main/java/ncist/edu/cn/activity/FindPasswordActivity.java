package ncist.edu.cn.activity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.UpdateListener;
import cn.smssdk.EventHandler;
import cn.smssdk.SMSSDK;
import ncist.edu.cn.R;
import ncist.edu.cn.entity.MyUser;
import ncist.edu.cn.utils.CommonUtils;
import ncist.edu.cn.utils.Constant;
import ncist.edu.cn.utils.NetworkUtils;
import ncist.edu.cn.utils.ValidatorUtil;

public class FindPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etMobile;
    private EditText etCode;
    private EditText etPassword;
    private Button btnGetCode;
    private Button btnSubmit;
    private EventHandler eventHandler;
    private Handler handler;
    private String mobile;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_password);
        // 初始化Mob短信功能
        SMSSDK.initSDK(this, Constant.MOB_APP_KEY, Constant.MOB_SECRET);
        init();
        handler = new Handler(new InnerCallBack());
        eventHandler = new EventHandler() {
            @Override
            public void afterEvent(int event, int result, Object data) {
                if (result == SMSSDK.RESULT_COMPLETE) {
                    //回调完成
                    if (event == SMSSDK.EVENT_SUBMIT_VERIFICATION_CODE) {
                        //提交验证码成功
                        Log.d("debug","验证码提交成功!");
                        Message.obtain(handler, SMSSDK.EVENT_SUBMIT_VERIFICATION_CODE).sendToTarget();
                    } else if (event == SMSSDK.EVENT_GET_VERIFICATION_CODE) {
                        //获取验证码成功
                       Log.d("debug","获取验证码成功!");
                    } else if (event == SMSSDK.EVENT_GET_SUPPORTED_COUNTRIES) {
                        //返回支持发送验证码的国家列表
                    }
                } else if(result == SMSSDK.RESULT_ERROR){
                    Throwable throwable = (Throwable) data;
                    throwable.printStackTrace();
                    JSONObject object = null;
                    try {
                        object = new JSONObject(throwable.getMessage());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String des = object.optString("detail");//错误描述
                    int status = object.optInt("status");//错误代码
                    if (status > 0 && !TextUtils.isEmpty(des)) {
                        CommonUtils.showToast(FindPasswordActivity.this, des);
                        return;
                    }
                }
            }
        };
        SMSSDK.registerEventHandler(eventHandler);
    }

    private class InnerCallBack implements Handler.Callback {

        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case SMSSDK.EVENT_SUBMIT_VERIFICATION_CODE:
                    findPassword(mobile,password);
                    break;
            }
            return false;
        }
    }

    //初始化控件
    private void init() {
        etMobile = (EditText) findViewById(R.id.find_password_et_mobile);
        etCode = (EditText) findViewById(R.id.find_password_et__code);
        etPassword = (EditText) findViewById(R.id.find_password_et_password);
        btnGetCode = (Button) findViewById(R.id.find_password_btn_get_code);
        btnSubmit = (Button) findViewById(R.id.find_password_btn_completed);
        btnGetCode.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
    }

    //找回密码合法性验证
    public boolean checkFindPasswordInput(String mobile, String password) {
        boolean isLegal = true;
        if(mobile.length() == 0){
            isLegal = false;
            CommonUtils.showToast(this,"手机号不能为空!");
        }
        else if (mobile.length() != 11) {
            isLegal = false;
            CommonUtils.showToast(this, "手机号长度必须为11位!");
        }
        if (password.length() < 6) {
            isLegal = false;
            CommonUtils.showToast(this, "密码长度必须大于等于6位!");
        }
        return isLegal;
    }

    //设置一个定时器
    public void CountDownTime() {
        btnGetCode.setClickable(false);
        new CountDownTimer(60000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                btnGetCode.setText("剩余:" + millisUntilFinished / 1000 + "秒");
            }

            @Override
            public void onFinish() {
                btnGetCode.setText("重新获取验证码");
                btnGetCode.setClickable(true);
            }
        }.start();
    }

    //设置密码
    public void findPassword(String mobile, final String password) {
        MyUser user = new MyUser();
        user.setMobile(mobile);
        BmobQuery<MyUser> query = new BmobQuery<MyUser>();
        query.addWhereEqualTo("mobile", mobile);
        query.findObjects(this, new FindListener<MyUser>() {
            @Override
            public void onSuccess(List<MyUser> list) {
                if (list != null && list.size() > 0) {
                    MyUser user = list.get(0);
                    user.setPassword(password);
                    user.update(FindPasswordActivity.this, user.getObjectId(), new UpdateListener() {
                        @Override
                        public void onSuccess() {
                            CommonUtils.showToast(FindPasswordActivity.this, "修改密码成功!");
                            finish();
                        }

                        @Override
                        public void onFailure(int i, String s) {
                            Log.e("error",i+s);
                            CommonUtils.showToast(FindPasswordActivity.this, "修改密码失败!" + s);
                        }
                    });
                }
            }

            @Override
            public void onError(int i, String s) {
                Log.e("error",i+s);
                CommonUtils.showToast(FindPasswordActivity.this, s);
            }
        });

    }

    @Override
    public void onClick(View v) {
        mobile = etMobile.getText().toString().trim();
        password = etPassword.getText().toString().trim();
        switch (v.getId()) {
            //获取验证码
            case R.id.find_password_btn_get_code:
                if(!NetworkUtils.isNetworkAvailable(this)){
                    CommonUtils.showToast(this,"网络不可用，请检查网络设置!");
                    return;
                }
                boolean isLeagl = checkFindPasswordInput(mobile, "123456");
                if (!isLeagl) {
                    return;
                }
                SMSSDK.getVerificationCode("86", mobile);
                CountDownTime();
                break;
            //提交密码和验证码
            case R.id.find_password_btn_completed:
                isLeagl = checkFindPasswordInput(mobile, password);
                if (!isLeagl) {
                    return;
                }
                String code = etCode.getText().toString();
                if(ValidatorUtil.isEmpty(code)){
                	CommonUtils.showToast(this,"验证码不能为空!");
                	return;
                }
                SMSSDK.submitVerificationCode("86", mobile, code);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        if (eventHandler != null) {
            SMSSDK.unregisterEventHandler(eventHandler);
            eventHandler = null;
        }
        super.onDestroy();
    }
}
